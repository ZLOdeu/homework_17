﻿#include <iostream>
using namespace std;

class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
    friend Vector operator + (Vector w, Vector v)
    {
        return Vector(w.x + v.x, w.y + v.y, w.z + v.z);
    }
};

int main()
{
    setlocale(LC_ALL, "rus");
    int x, y, z;
    cout << "Введите значение X: ";
    cin >> x;
    cout << "Введите значение Y: ";
    cin >> y;
    cout << "Введите значение Z: ";
    cin >> z;

    Vector v(x, y, z);
};

